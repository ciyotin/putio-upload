import putiopy
import os
import time
import sys
import getopt

def createClient(OAUTH_TOKEN):
    try:
        client = putiopy.Client(OAUTH_TOKEN, use_retry=True, timeout=50)
    except:
        print("Baglanti saglanamadi")
        sys.exit(1)

    return client

def createFolderStructure(putioClient, dstFolderId, srcFolder):
    folderName = os.path.basename(srcFolder)

    try:
        newFolder = putioClient.File.create_folder(parent_id=dstFolderId, name=folderName)
    except Exception as e:
        if e.type == "NAME_ALREADY_EXIST":
            print("Klasor zaten var")
            sys.exit(1)
        else:
            print("Klasor olusturulamadi")
            sys.exit(1)

    createSubFoldersAndUploadFiles(putioClient, newFolder.id, srcFolder)


def createSubFoldersAndUploadFiles(putioClient, dstFolderId, srcFolder):
    for folderorFile in os.listdir(srcFolder):
        if os.path.isdir(srcFolder+"/"+folderorFile):
            newFolder = putioClient.File.create_folder(parent_id=dstFolderId, name=folderorFile)
            createSubFoldersAndUploadFiles(putioClient, newFolder.id, srcFolder+"/"+folderorFile)
        else:
            for i in range(0, 10):
                try:
                    uploadFile(putioClient, srcFolder + "/" + folderorFile, dstFolderId)
                except Exception as e:
                    print(srcFolder + "/" + folderorFile + "----", end='')
                    print(e)
                    time.sleep(i)
                    continue
                break



def uploadFile(putioClient,srcFile,dstFolderId):
    putioClient.File.upload(srcFile, parent_id=dstFolderId)

def getDstFolder(putioClient):

    uploadFolderId = 0
    selectedFolder = None
    folderTree = [0]

    while selectedFolder != 0:
        folders = None
        try:
            folders = putioClient.File.list(uploadFolderId, file_type='FOLDER')
        except:
            print("Baglanti saglanamadi")
            sys.exit(1)

        folderNames = []
        folderIds = []

        for i in folders:
            folderNames.append(i.name)
            folderIds.append(i.id)

        if len(folderTree) > 1:
            print("-1\t-\tOnceki dizin")
        print("0\t-\tSimdiki dizin")

        for i in range(0, len(folderNames)):
            print(str(i + 1) + "\t-\t" + folderNames[i])

        while True:
            try:
                selectedFolder = int(input("Klasor seciniz\n"))
                if (selectedFolder < -1) or selectedFolder > len(folderNames):
                    print("Boyle bir secenek yok lutfen tekrar giriniz.")
                    continue
                break
            except:
                print("Yanlis girdiniz, lutfen sayi giriniz")

        if selectedFolder != 0:
            if selectedFolder == -1:
                uploadFolderId = folderTree[-2]
                del folderTree[-1]
            else:
                uploadFolderId = folderIds[selectedFolder - 1]
                folderTree.append(uploadFolderId)

    return uploadFolderId


def main(argv):
    helpText="NAME\n\tputio-upload\t-\tUploads folder to your put.io account\n\nSYNOPSIS\n\t\
python putio-upload.py [OPTION]... [FILE|TOKEN]...\n\n\
DESCRIPTION\n\t\
Upload selected folder to selected put.io folder\n\n\t\
-t <OAUTH TOKEN>\n\t-i <INPUT FOLDER PATH>\n\n\
USAGE\n\t\
python putio-upload.py -t \"T5F4CD7QNQXZE35GIFXX\" -i \"C:\\mini-project\"\n"

    OAUTH_TOKEN = None
    srcFolder = None
    try:
        opts, args = getopt.getopt(argv, "ht:i:", ["t=", "i="])
    except getopt.GetoptError:
        print(helpText)
        sys.exit(1)
    for opt, arg in opts:
        if opt == '-h':
            print(helpText)
            sys.exit(1)
        elif opt in ("-t"):
            OAUTH_TOKEN = arg
        elif opt in ("-i"):
            srcFolder = arg

    if (OAUTH_TOKEN is None) or (srcFolder is None):
        print(helpText)
        sys.exit(1)

    if not os.path.isdir(srcFolder):
        print("Verilen klasor hatali")
        sys.exit(1)

    #CLIENT_ID=5467
    #CLIENT_SECRET="HUT6CG5XKOQA2BWSFI3M"
    #OAUTH_TOKEN = "T5F4CD7QNQXZE35GIFBP"
    #srcFolder="C:\\mini-project"

    putioClient = createClient(OAUTH_TOKEN)

    dstFolderId = getDstFolder(putioClient)

    createFolderStructure(putioClient, dstFolderId, srcFolder)
    print("Klasor basariyla karsiya yuklendi")


if __name__ == "__main__":
    main(sys.argv[1:])
